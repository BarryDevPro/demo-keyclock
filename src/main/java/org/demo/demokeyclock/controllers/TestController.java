package org.demo.demokeyclock.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class TestController {

    @GetMapping("/test")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<String> test(){
        return ResponseEntity.ok("Hello World");
    }

    @GetMapping("/auth")
    public Authentication authentication(Authentication auth){
        return auth;
    }

    @GetMapping("/test-user")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<String> testUser(){
        return ResponseEntity.ok("Hello user");
    }
}
